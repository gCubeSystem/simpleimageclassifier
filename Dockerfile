# Base
FROM ubuntu:20.04

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y python3 python3-pip wget
 

# Istall deps
COPY ./requirements.txt /
RUN pip3 install -r requirements.txt 

RUN pip3 install torch==1.10.1+cpu torchvision==0.11.2+cpu torchaudio==0.10.1+cpu -f https://download.pytorch.org/whl/cpu/torch_stable.html 
RUN pip3 install detectron2 -f \
  https://dl.fbaipublicfiles.com/detectron2/wheels/cpu/torch1.10/index.html

# Install dist package sortapp
COPY ./dist/simpleimageclassifier-1.0.0.tar.gz /

RUN pip3 install simpleimageclassifier-1.0.0.tar.gz 

COPY canegatto.jpg /
#
#RUN rm sortapp-1.0.0.tar.gz
#RUN rm requirements.txt
#RUN rm -r /root/.cache

### Alternative ###
# Create a working directory and Bundle app source
# WORKDIR /simpleimageclassifier
# COPY src/simpleimageclassifier /simpleimageclassifier

# Copy all subfolder
#ADD . /

# Autorun
# CMD [ "python3", "./simpleimageclassifier.py" ]
