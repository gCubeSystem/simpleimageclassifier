# SimpleImageClassifier

SimpleImageClassifier is a simple example that allows you to classify a image jpg in input.
This example is based on [Detectron2](https://gitub.com/facebookresearch/detectron2 ) that is a Facebook AI Research's.
Starting from this example, you can first create an installable package via pip3 and then a docker image in which it is installed the created package.
The package declares the simpleimageclassifier command as entrypoint.
So once the package is installed you can use this command at command line.
Also, you can run it as a module, for example:

```
$ cd src
$ python3 -m simpleimageclassifier --config-file simpleimageclassifier/configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml --input ../canegatto.jpg --output canegatto_out.jpg --opts MODEL.DEVICE cpu MODEL.WEIGHTS detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl

```
The image created in this way can be executed in a container with the following command using an jpg file placed in it:

```
docker run -i -t --rm --name simpleimageclassifier-cont simpleimageclassifier simpleimageclassifier --config-file /usr/local/lib/python3.8/dist-packages/simpleimageclassifier/configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml --input <image.jpg> --output <image_out.jpg> --opts MODEL.DEVICE cpu MODEL.WEIGHTS detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl
```
You can also run it directly from the container shell:

```
$ docker run -i -t --rm --name simpleimageclassifier-cont simpleimageclassifier bash

root@7f371ac6f420:/# simpleimageclassifier --config-file /usr/local/lib/python3.8/dist-packages/detectron2/model_zoo/configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml --input canegatto.jpg --output canegatto_out.jpg --opts MODEL.DEVICE cpu MODEL.WEIGHTS detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl

```
Note the model set by --config-file can be take from detectron2 local lib in this case.


To be able to create an image from this application you need to have Docker and Docker-Compose installed on your machine and the relative python packages, see: 
[Docker](https://docs.docker.com/engine/),
[Docker-Compose](https://docs.docker.com/compose/install/) and
[Docker Package for Python](https://pypi.org/project/docker/).

## Useful Commands

### Create Distribution Package
```
python3 setup.py sdist --formats=gztar
```
### Create Docker Image 
```
docker build -t simpleimageclassifier .
```

### Save Docker Image in file
```
docker save simpleimageclassifier | gzip > simpleimageclassifier.tar.gz
```

### Publish Docker Image on DockerHub
Re-tagging an existing local image: 

```
docker tag simpleimageclassifier <hub-user>/<repo-name>[:<tag>]
```

Login in DockerHub(use your Docker ID):

```
docker login
```

Now you can push this repository to the registry designated by its name or tag:

```
docker push <hub-user>/<repo-name>:<tag>
```

Then logout for security:

```
docker logout
```


## Authors

* **Giancarlo Panichi** ([ORCID](http://orcid.org/0000-0001-8375-6644)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)



## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

